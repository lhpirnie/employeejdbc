package qa.emp;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAOImpl implements EmployeeDAO {
    private String host;
    private String username;
    private String password;
    private Connection con;

    public EmployeeDAOImpl(String host) {
        this.host = host;
    }

	public void login(String username, String password) {
        this.username = username;
        this.password = password;

    }

	@Override
	public List<Employee> getAllEmployees() {
		return new ArrayList<>();
	}

	@Override
	public List<Employee> getEmployeesByNameMatch(String nameFragment) {
		return new ArrayList<>();
	}

	@Override
	public Employee getEmployeeById(int id) {
		return null;
	}

	@Override
	public int getNextFreeId() {
		int lastIndex = 100000;
		try {
			Statement stmt = con.createStatement();
			ResultSet res = stmt.executeQuery("SELECT MAX(id) FROM employees");
			if (res.next()) {
				lastIndex = res.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		lastIndex++;  // next free space...
		return lastIndex;
	}


	@Override
	public boolean insertEmployee(Employee employee) {
		return false;
	}

	@Override
	public boolean deleteEmployee(Employee employee) {
		return false;
	}

	@Override
	public void logout() {
		
	}



}
