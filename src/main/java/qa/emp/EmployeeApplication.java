package qa.emp;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class EmployeeApplication {

    private String userName = "root";
    private String password = "password";
    private EmployeeDAO dao;

    public static void main(String[] args) {
    	if (args.length==0) {
    		System.out.println("Need to supply host as command-line parameter");
    		System.exit(0);
    	}
    	EmployeeDAO dao = new EmployeeDAOImpl(args[0]);
    	final EmployeeApplication app = new EmployeeApplication(dao);
    	final EmployeeFrame frame = new EmployeeFrame(app);
        frame.addWindowListener(new WindowAdapter() {
                            	public void windowClosing(WindowEvent we) {
                                	frame.shutDown();
                                	System.exit(0);
                                }
                            });
        frame.setSize(600, 450);
        frame.setVisible(true);
    }
    
	public EmployeeApplication(EmployeeDAO dao) {
		this.dao = dao;
	} 

	// returns a formatted String representation of the current employees
    public String getEmployees() {
    	StringBuilder sb = new StringBuilder(200);
       	List<Employee> employees = dao.getAllEmployees();
    	for (Employee emp : employees) {
    		sb.append(emp.getId() + "\t" + emp.getFullname() + "\t" + emp.getAge() + "\n");
    	}
        return sb.toString();
    }

    public boolean removeEmployee(String name) {
    	boolean result = false;   	
    	List<Employee> employees = dao.getAllEmployees();
    	for (Employee emp : employees) {
    		if (name.equals(emp.getFullname())) {
    			result = dao.deleteEmployee(emp);
    			break;
    		}
    	}    	
        return result;
    }

    public boolean addEmployee(String name, int age) {
    	boolean result = false;
    	String[] nameSegments = name.split(" ");
    	String firstname = "";
    	String lastname = "";
    	if (nameSegments.length >= 2) {
    		firstname = nameSegments[0];
    		lastname = nameSegments[1];
    	} else if (nameSegments.length == 1) {
    		firstname = "?";
    		lastname = nameSegments[0];
    	} else {
    		firstname = "?";
    		lastname = "?";
    	}
    	
    	int id = dao.getNextFreeId();
    	Employee emp = new Employee(id, firstname, lastname, age, 100000.0, "ADMIN");
    	result = dao.insertEmployee(emp);
    	
        return result;
    }

    public void shutDown() {
    	dao.logout();
    }
    
}

