package qa.emp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class EmployeeFrame extends JFrame {
	private JPanel titlePanel, westPanel, addEmpPanel, removePanel, addPanel,
    			  addButtonPanel, resultsPanel, outputPanel, removeEmpPanel,
                  removeButtonPanel, showPanel, framePanel,
                  removeNameTxtPanel;

	private JLabel titleLabel, addNewEmpLabel, addEmpAgePanel, showLabel,
    			  statusLabel, removeNameLabel;

	private JTextField addEmpNameTxt, addEmpAgeTxt, removeNameTxt;

	private JButton remove, add;

    private JTextArea resultTxt, outputTxt;

    private Container theFrame;

    private EmployeeApplication employeeApp;


    public EmployeeFrame(EmployeeApplication app) {
    	employeeApp = app;
    	layoutComponents();
        registerListeners();
        showEmployees();
    }

    public void shutDown() {
      try {
        employeeApp.shutDown();
        this.dispose();
      } catch (Exception e) {
        e.printStackTrace(System.err);
      }
    }

    public void layoutComponents() {
    	theFrame = getContentPane();
    	theFrame.setLayout(new BorderLayout(10, 10));

        theFrame.add(framePanel = new JPanel(new BorderLayout(10, 10)));
        Border frameBorder = BorderFactory.createLineBorder(Color.blue, 5);
        framePanel.setBorder(frameBorder);


    	// North sector
		titlePanel = new JPanel();
		titleLabel = new JLabel("Employee Application");
		titleLabel.setFont(new java.awt.Font("Serif", 0, 25));
        titlePanel.add(titleLabel, null);

        framePanel.add(titlePanel, BorderLayout.NORTH);

        // West sector
		westPanel = new JPanel(new GridLayout(2, 1));

        // adding an employee
        addPanel = new JPanel(new BorderLayout());
        Border eb = BorderFactory.createEtchedBorder();
		TitledBorder addBorder = BorderFactory.createTitledBorder(eb, "Add Employee");
        addPanel.setBorder(addBorder);

		addEmpPanel = new JPanel(new GridLayout(4, 1));
        addEmpPanel.add(addNewEmpLabel = new JLabel("Enter name:"));
		addEmpPanel.add(addEmpNameTxt = new JTextField());
		addEmpPanel.add(addEmpAgePanel = new JLabel("Enter age:"));
		addEmpPanel.add(addEmpAgeTxt = new JTextField());
        addPanel.add(addEmpPanel, BorderLayout.CENTER);

        addButtonPanel = new JPanel();
        addButtonPanel.add(add = new JButton("Add"));
		addPanel.add(addButtonPanel, BorderLayout.SOUTH);

        westPanel.add(addPanel);

        // removing a member
		removePanel = new JPanel(new BorderLayout());
        eb = BorderFactory.createEtchedBorder();
		TitledBorder removeBorder = BorderFactory.createTitledBorder(eb, "Remove Employee");
        removePanel.setBorder(removeBorder);

		removeEmpPanel = new JPanel(new GridLayout(2, 1));
		removeEmpPanel.add(removeNameLabel = new JLabel("Enter name:"));

        // local class to allow you to inset a component in a Panel
        class TextPanel extends JPanel {
        	public TextPanel(LayoutManager lm) {
            	super(lm);
            }

        	public Insets getInsets() {
            	return new Insets(5, 0, 5, 0);
            }
        }
        removeNameTxtPanel = new TextPanel(new BorderLayout());
		removeNameTxtPanel.add(removeNameTxt = new JTextField(), BorderLayout.CENTER);
        removeEmpPanel.add(removeNameTxtPanel);
        removePanel.add(removeEmpPanel, BorderLayout.CENTER);

		removeButtonPanel = new JPanel();
		removeButtonPanel.add(remove = new JButton("remove member"));
        removePanel.add(removeButtonPanel, BorderLayout.SOUTH);

        westPanel.add(removePanel);

        framePanel.add(westPanel, BorderLayout.WEST);

        // Center sector
        resultsPanel = new JPanel(new BorderLayout());

        showPanel = new JPanel();
        showPanel.add(showLabel = new JLabel("All the current Employees"));
        resultsPanel.add(showPanel, BorderLayout.NORTH);

        resultsPanel.add(resultTxt = new JTextArea(), BorderLayout.CENTER);

        framePanel.add(resultsPanel, BorderLayout.CENTER);


        // South sector
		outputPanel = new JPanel();
        eb = BorderFactory.createEtchedBorder();
		TitledBorder outputBorder = BorderFactory.createTitledBorder(eb, "Status");
        outputPanel.setBorder(outputBorder);

		outputPanel.add(outputTxt = new JTextArea(2, 40));

        framePanel.add(outputPanel, BorderLayout.SOUTH);

        addEmpNameTxt.grabFocus();
    }

    public void registerListeners() {
    	// anonymous class to respond to an "add employee" request
    	add.addActionListener(new ActionListener() {
                                	public void actionPerformed(ActionEvent ae) {
                                     	String name = addEmpNameTxt.getText();
                                        String strAge = addEmpAgeTxt.getText();
                                        if (isValidEntry(name, strAge)) {
                                        	int age = 0;
                                            try {
                                        		age = Integer.parseInt(strAge);
                                        		boolean success = employeeApp.addEmployee(name, age);
                                            	if (success) {
                                            		outputTxt.setText("Employee Added");
	                                            	addEmpNameTxt.setText("");
	                                            	addEmpAgeTxt.setText("");
	                                            	addEmpNameTxt.grabFocus();
	                                            	showEmployees();
                                            	} else {
                                            		outputTxt.setText("Employee Not Added");
                                            	}
                                            } catch (NumberFormatException ne) {
                                            	outputTxt.setText("Age must be a number");
                                                addEmpAgeTxt.grabFocus();
                                            }
                                        }
                                    }
                                });

        // anonymous class to respond to a "remove employee" request
        remove.addActionListener(new ActionListener() {
                                	public void actionPerformed(ActionEvent ae) {
                                    	String name = removeNameTxt.getText();
                                        if (isValidEntry(name)) {
                                        	if (employeeApp.removeEmployee(name)) {
                                        		outputTxt.setText("Employee removed");
                                                removeNameTxt.setText("");
                                                removeNameTxt.grabFocus();
                                                showEmployees();
                                            } else {
                                        		outputTxt.setText("No such Employee");
                                            }
                                        } else {
                                        	removeNameTxt.grabFocus();
                                        }
                                    }
                                });
    }

    // helper method to format and display the current employees
    private void showEmployees() {
    	resultTxt.setFont(new Font("Courier", Font.BOLD, 18));
        resultTxt.setText("IDNUM\tNAME\t\tAGE\n");
        resultTxt.setFont(new Font("Courier", Font.PLAIN, 15));
    	String mems = employeeApp.getEmployees();
    	resultTxt.append(mems);
    }

    // helper method to validate a name and age
    private boolean isValidEntry(String name, String age) {
    	boolean result = false;
    	if (isValidEntry(name)) {
        	if (age.equals("")) {
            	outputTxt.setText("Must enter an age");
                addEmpAgeTxt.grabFocus();
            } else {
            	result = true;
            }
        } else {
        	if (age.equals("")) {
            	outputTxt.append(" and age");
            }
        }
        return result;
    }

    // overloaded helper to validate a name
    private boolean isValidEntry(String name) {
    	if (name.equals("")) {
        	outputTxt.setText("Must enter a name");
            addEmpNameTxt.grabFocus();
        	return false;
        } else {
            return true;
        }
    }

    public Insets getInsets() {
    	return new Insets(10, 10, 10, 10);
    }



}